
# provider.tf

# Specify the provider and access details
provider "aws" {
#  shared_credentials_files = ["$HOME/.aws/credentials"]
#  profile                 = "gm-saa-c02"
  region                  = var.aws_region
}