# Passo a passo

## 1. Conteineirização da Aplicação

De forma a conseguir executar a aplicação localmente, o primeiro passo foi conteinerizar a aplicação. Nenhuma otimização ou utilização de melhores práticas foi pensada neste momento.

```dockerfile
FROM python:3.7.4-slim

WORKDIR /usr/app

COPY app .

RUN pip install -r requirements.txt

EXPOSE 8000

CMD [ "gunicorn", "-b", "0.0.0.0:8000", "--log-level", "debug", "api:app" ]


```

### Docker build

```
docker build . -t desafio-esx-globo
```

### Docker run

```
docker run -p 8000:8000 desafio-esx-globo
```

## 2. Desenvolvimento Local - Criação de Docker-compose file

Com o intuito de facilitar a execução local para os devs e criar uma estrutura para execução de testes futuros pelo pipeline de CI, um `docker-compose.yml`. Isto torna etapa de build e execução dos serviços mais direta, invés de exigir a passagem de grandes quantidades de parâmetros nos comandos do `docker build` e `docker run`. O docker-compose também proporciona a utilização de arquivo `.env` que podem ser utilizados tanto pelo build (ARG) quanto pela execução (ENV).

Os parâmetros de execução foram movidos para um arquivo `.env`, que foi incluído no `.gitignore` por boas práticas (cada dev deverá possuir o seu).

### Hot Reload

Para acelerar o desenvolvimento, o docker-compose executa a api com a opção `--reload` e a pasta `app` é montada em um volume do container. Isto faz com que a API seja automaticamente recarregada em qualquer modificação dos arquivos `.py`.

## 3. Pipeline CI e melhorias no Dockerfile

Foi pensado em uma estrutura de branchs com as branchs **main** e **stable** sempre presentes. Branchs acessórias ficam livres para os desenvolvedores (podem seguir padrões como o gitflow).

### Branchs

#### Main

Contém código atual de stagging. Dispara deploy em staging. Somente Maniteiners podem comitar direto e realizar merge-requests.

#### Stable

Contém código de produção, sempre com tags. Ninguém pode comitar direto e apenas Mainteiners podem realizar merge. Pipeline disparado no ato de tageamento seguindo padrão SEMVER.

### Pipeline

Foi elaborado um pipeline de CI com os seguintes estágios:

- check - apenas na branch stable. Verifica se tag está no padrão SEMVER.
- verify - Realiza o lint do Dockerfile em busca de garantir melhores práticas e verifica pode safaty das libs.
- test - Testes unitários (pytest) e BDD (behave)
- build - build da imagem docker
- push-image - Push para o container registry do gitlab
- deploy - deploy em staging (branch main, todo comit, automático), e em prod (manualmente, tag padrão SEMVER na branch stable)

### Melhorias no Dockerfile

Com o intuito de melhor organizar o Dockerfile e impor algumas melhores práticas, foi alterado a imagem base para o **alpine** e a utilização de múltiplos estágios no `Dockerfile`. Isto diminui consideravelmente o tamanho final da imagem do container. Entretanto, a compilação de algumas libs python podem exigir mais cuidado devido ao fato do alpine utilizar o `muslc` em detrimento do `libc`.

### Criação de Makefile

A criação de um Makefile para executar os comandos de docker build deixa mais clara e organizada a utilização dos múltiplos etapas do pipeline pelos scripts do `gitlab-ci.yml` e execuções locais.

### Utilização do poetry para as dependências

Foi alterado a forma de registrar as dependências do simples `requirements.txt` para a utilização do poetry, que possui recursos mais avançados para a gestão de libs.

### Testes Unitários e BDD

Foi criado a etapa de testes unitários e BDD com exemplo de testes que os desenvolvedores poderiam implementar. No futuro, seria interessante execução de testes unitários do app, testes e2e, de integração, etc. Estágios criados para demosntração do pipeline.

## 4. Pipeline CD

Foi optado por utilizar o AWS como provider para a solução, por ser a provedora com maior marketshare. Como ferramenta para para automantismo do deploy e provisionamento, foi elegido o **Terraform**. Inicialmente, foi avaliado usar o próprio CloudFormation da AWS mas deste modo a solução não seria agóstica de provedor de serviços cloud. A escolha do Terraform foi motivada por ser uma das ferramentas mais poderosas e amplamente divulgadas. A escolha de ferramentas com uma base de usuários maior é benéfica em relação à diferentes aspectos, como uma maior disponibilidade de mão de obra no mercado, ferramentas, material de estudo como cursos, tutoriais etc.

Para o deploy, foi inicialmente pensado a utilização do serverless com um API gateway, porém oi deploy através do AWS ECS Fargate trás vantagens em relação à esta opção. A decisão entre ambas as tecnologias exige muito mais informações para avaliar qual é a melhor para um caso real específico. Como o intuito deste projeto é demonstrar capacidade técnica na área de DevOps, foi optado ir pela solução que é, dentre as duas, mais complexa, porém ainda simples o bastante. Um caso um pouco mais complexo seria utilizar um pipeline de CD que envolveria criar um helmcharts, publica-lo em um repositório e fazer um deploy em um cluster kubernetes com um deployment, um service e um ingress, utilizando um loadbalancer do cluster como o traefik por exemplo.

Para iniciar o projeto terraform, foi tomado como base um repositório opensource (https://github.com/AjeetK/terraform-ecs-fargate) e feito as modificações necessárias.

# Melhorias Futuras

- Realizar o push do estágio "builder" do Dockerfile de múltiplos estágios para o container registry, e usar este como base para os outros estágios. Desta forma é economizado o tempo de cada job no pipeline por conta da demora de instalação das libs no Alpine.
- Aplicar melhores práticas para executar o guinucorn em container.
- Documentar API com OpenAPI e utilizar o schema para testes de API REST baseado na documentação `openapi.yml`
- Exclusão do loadbalancer na aws prende `terraform destroy` (https://github.com/hashicorp/terraform-provider-aws/issues/3414). Há workarround que ultrapassa a motivação deste exercício.
- Configurar CloudWatch para monitorar aplicação
- Configurar integrações dos recursos do gitlab
