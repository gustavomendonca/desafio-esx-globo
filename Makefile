# Baseado em https://github.com/garethr/multi-stage-build-example

IMAGE=desafio-esx-globo
BUILD=docker build -f docker/Dockerfile.alpine --target
RUN=docker run --rm
APP_PORT=8000
TESTS=structure-tests.yaml

all: verify dev

verify: lint test-unit test-bdd check

lint:
	cat docker/Dockerfile.alpine | docker run --rm -i hadolint/hadolint hadolint --ignore DL3018 --ignore DL3013 -

test-bdd:
	$(BUILD) bdd-test -t $(IMAGE) .
	
test-unit:
	$(BUILD) unit-test --no-cache -t $(IMAGE) .

check:
	$(BUILD) check -t $(IMAGE) .

dev:
	$(BUILD) dev -t $(IMAGE) .
	$(RUN) -p $(APP_PORT):5000 $(IMAGE)

.prod-build:
	$(BUILD) prod -t $(IMAGE) .

prod: .prod-build
	$(RUN) -p $(APP_PORT):8000 $(IMAGE)

.PHONY: all verify lint test-unit test-bdd check dev prod .prod-build
